from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from .forms import *
import json
from django.shortcuts import get_object_or_404


def index(request):
    return render(request, 'main/index.html')


def home(request):
    form_list = Form.objects.all()
    context = {
        'form_list': form_list,
    }
    return render(request, 'main/home.html', context)


def edit(request):
    return render(request, 'main/edit.html')


def form_save(request):
    if request.method == "POST":
        form = AppForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            description = form.cleaned_data['description']
            f = Form()
            f.title = title
            f.description = description
            f.save()
            data = {
                'id': f.id,
                'title': f.title,
                'description': f.description
            }
            return HttpResponse(json.dumps(data), content_type='application/json', status=201)
    else:
        return HttpResponse(status=405)


def short_ques_save(request):
    if request.method == "POST":
        sq_form = ShortQuestionForm(request.POST)
        if sq_form.is_valid():
            form_object = sq_form.cleaned_data['form']
            short_question_text = sq_form.cleaned_data['short_question_text']
            sq = ShortQuestion()
            sq.form = form_object
            sq.short_question_text = short_question_text
            sq.save()
            data = {
                'id': sq.id,
                'short_question_text': sq.short_question_text,
            }
            return HttpResponse(json.dumps(data), content_type='application/json', status=201)
    else:
        return HttpResponse(status=405)


def multiple__ques_save(request):
    if request.method == "POST":
        mcq_form = MultipleQuestionForm(request.POST)
        if mcq_form.is_valid():
            form_object = mcq_form.cleaned_data['form']
            multiple_question_text = mcq_form.cleaned_data['multiple_choice_question_text']
            mcq = MultipleChoiceQuestion()
            mcq.form = form_object
            mcq.mc_question_text = multiple_question_text
            mcq.save()
            # import ipdb;ipdb.set_trace()
            for opt in mcq_form.cleaned_data['options']:
                choice = MultipleChoices()
                choice.choice_text = opt
                choice.mc_question = mcq
                choice.save()
            data = {
                'id': mcq.id,
                'mc_question_text': mcq.mc_question_text,
                'choices': [{
                    'id': choice.id,
                    'choice_text': choice.choice_text
                } for choice in MultipleChoices.objects.filter(mc_question=mcq)]

            }
            return HttpResponse(json.dumps(data), content_type='application/json', status=201)
    else:
        return HttpResponse(status=405)


def response(request, form_id):
    form = Form.objects.get(pk=form_id)
    sq_list = ShortQuestion.objects.filter(form=form)
    mcq_list = MultipleChoiceQuestion.objects.filter(form=form)
    context = {
        'form': form,
        'sq_list': sq_list,
        'mcq_list': [{
            'mcq': mcq,
            'choices': MultipleChoices.objects.filter(mc_question=mcq)
        } for mcq in mcq_list],
    }
    return render(request, 'main/response.html', context)


def response_save(request):
    if request.method == "POST":
        form_id = request.POST.get('form')
        form_object = Form.objects.get(id=form_id)
        resp = Response()
        resp.form = form_object
        resp.save()
        short_response_list = json.loads(request.POST.get('shortResponses'))
        for sr in short_response_list:
            short_response = ShortResponse()
            short_response.short_question = ShortQuestion.objects.get(id=sr['questionId'])
            short_response.short_response_text = sr['textResponse']
            short_response.response = resp
            short_response.save()
        multiple_choice_response_list = json.loads(request.POST.get('multipleChoiceResponses'))
        for mcqr in multiple_choice_response_list:
            for choice in mcqr['choices']:
                multiple_choice_response = MultipleChoiceResponse()
                multiple_choice_response.response = resp
                multiple_choice_response.mc_question = MultipleChoiceQuestion.objects.get(id=mcqr['questionId'])
                multiple_choice_response.choices = MultipleChoices.objects.get(id=choice['choiceId'])
                multiple_choice_response.accepted = choice['checked']
                multiple_choice_response.save()

        data = {
            'response': resp.id,
            'shortResponses': [{
                'id': shortResponse.id,
                'short_response_text': shortResponse.short_response_text
            } for shortResponse in ShortResponse.objects.filter(response=resp)],
            'multipleChoiceResponses': [{
                'id': multipleChoiceResponse.id,
                'accepted': multipleChoiceResponse.accepted
            } for multipleChoiceResponse in MultipleChoiceResponse.objects.filter(response=resp)]
        }
        return HttpResponse(json.dumps(data), content_type='application/json', status=201)
    else:
        return HttpResponse(status=405)


def result(request, form_id):
    form = Form.objects.get(pk=form_id)
    sq_list = ShortQuestion.objects.filter(form=form)
    responses = Response.objects.filter(form=form)
    count = responses.count()
    context = {
        'form': form,
        'sq_list': sq_list,
        'responses': responses,
        'count': count,
    }
    return render(request, 'main/response_list.html', context)


def response_detail(request, response_id):
    response = Response.objects.get(id=response_id)
    short_responses = ShortResponse.objects.filter(response=response)
    context = {
        'short_responses': short_responses,
        'form': response.form,
    }
    return render(request, 'main/response_detail.html', context)
