from django.contrib import admin
from .models import *
# Register your models here.


admin.site.register(Form)
admin.site.register(ShortQuestion)
admin.site.register(MultipleChoiceQuestion)
admin.site.register(MultipleChoices)
admin.site.register(Response)
admin.site.register(ShortResponse)
admin.site.register(MultipleChoiceResponse)
admin.site.register(MultipleChoiceOtherResponse)
