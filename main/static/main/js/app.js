/**
 * Created by nidhi on 28/1/17.
 */
globals = {};
globals.FormId = globals.FormId || -1;

$(document).ready(function() {
    $('#add_question_button').hide();
    var cid = 0;
    // var formResponseData;
    // Ajax call to save form data
    $('#appForm').submit(function(event) {
        event.preventDefault();
        var formData = {
            'title': $('input[name=title]').val(),
            'description': $('input[name=description]').val(),
            'csrfmiddlewaretoken':$(':input[name=csrfmiddlewaretoken]').val()
        };
        $.ajax({
            url: '/api/form/save/',
            type: 'POST',
            data: formData,
        }).fail(function(){
            console.log("Request Failed");
        }).done(function(data){
            console.log("Success");
            console.log(data);
            globals.FormId = data.id;
            formResponseData = {
                'id': data.id,
                'title': data.title,
                'description': data.description,
            };
            $('#title').prop('disabled', true);
            $('#desc').prop('disabled', true);
            $('#formSave').hide();
            $('#add_question_button').show();

            document.getElementById('responseUrl').innerHTML = '<a target="_blank" href="/api/response/' + data.id + '/">http://' + window.location.hostname + '/api/response/' + data.id + '/</a>';
            document.getElementById('resultsUrl').innerHTML = '<a target="_blank" href="/api/result/' + data.id + '/">http://' + window.location.hostname + '/api/result/' + data.id + '/</a>';
            add_question(formResponseData);
        });
    });

    //Load jquery template to add general question in form
    var add_question = function(formResponseData){
        $('#add_question_button').on('click', function (event) {
            $('#add_question_button').hide();
            cid += 1;
            // General Question template holder
            $('#questions_holder').loadTemplate('#general_question_template', {
                general_question_cid: 'general_question_' + cid
            }, {
                append: true
            });
            // Default Short question template
            addSQ(cid);

            // Template swap on question type change
            setSelectChange(cid);
        });
    };

    var setSelectChange = function (cid) {
        $('#' + 'general_question_' + cid).find('.question_type_select').change(function() {
            if ($(this).val() === 'SQ') {
                addSQ(cid);
            } else {
                addMCQ(cid);
            }
        });
    };

    var addSQ = function (cid) {
        var gqCid = 'general_question_' + cid;
        // Load Short Question template
        $('#' + gqCid).find('.actual_question_holder').loadTemplate('#short_question_template', {
            // general_question_cid: 'general_question_' + cid
        }, {
            // append: true
        });

        //Ajax call to save short question type
        $('#' + gqCid).find('.actual_question_holder').find('form').submit(function(event) {
            event.preventDefault();
            console.log('this will be called after submitting question' + $('input[name=formId]').val());
            var formData = {
                'form': globals.FormId,
                'short_question_text': $(this).find('#shortQues').val(),
                'csrfmiddlewaretoken': $(this).find(':input[name=csrfmiddlewaretoken]').val()
            };
            $.ajax({
                url: '/api/shortQues/save/',
                type: 'POST',
                data: formData,
            }).fail(function(){
                console.log("Error");
            }).done(function(data){
                console.log("success");
                console.log(data);
                // Disabling question type selector
                $('#' + gqCid).find('.question_type_select').prop('disabled', true);
                
                $('#' + gqCid).find('.actual_question_holder').find('form').find('button').hide();
                $('#' + gqCid).find('.actual_question_holder').find('form').find('#shortQues').prop('disabled', true);
                $('#add_question_button').show();
            });
        });
    };

    var addMCQ = function (cid) {
        console.log('addMCQ' + cid);
        ccid = 0;
        var gqCid = 'general_question_' + cid;
        // Load Multiple Choice Question Template
        $('#' + gqCid).find('.actual_question_holder').loadTemplate('#multiple_choice_question_template', {
            // general_question_cid: 'general_question_' + cid
        }, {
            // append: true
        });
        // Closure for adding a choice
        var addChoice = function (ccid) {
            // choice_checkbox_template
            $('#' + gqCid).find('.actual_question_holder').find('form').find('#choices_holder').loadTemplate('#choice_checkbox_template', {
                option_ccid: 'option_' + ccid
            }, {
                append: true
            });
        };
        // Adding one mandatory option
        addChoice(ccid);
        // Adding more option on click of "Add Option Button"
        $('#' + gqCid).find('.actual_question_holder').find('form').find('#addChoiceBtn').click(function() {
            ccid += 1;
            addChoice(ccid);
        });

        //Ajax call to save multiple question type
        $('#' + gqCid).find('.actual_question_holder').find('form').submit(function(event) {
            event.preventDefault();
            var choices = [];
            $('#' + gqCid).find('.actual_question_holder').find('form').find('#choices_holder').find(':input').each(function(index, element) {
                console.log($(element).val());
                choices.push($(element).val());
            });

            var formData = {
                'form': globals.FormId,
                'multiple_choice_question_text': $(this).find('#multipleChoiceQues').val(),
                'csrfmiddlewaretoken': $(this).find(':input[name=csrfmiddlewaretoken]').val(),
                'options': JSON.stringify(choices)
            };
            $.ajax({
                url: '/api/multipleQues/save/',
                type: 'POST',
                data: formData,
            }).fail(function(){
                console.log("Error");
            }).done(function(data){
                console.log("success");
                console.log(data);
                // Disabling question type selector
                $('#' + gqCid).find('.question_type_select').prop('disabled', true);

                $('#' + gqCid).find('.actual_question_holder').find('form').find('button').hide();
                $('#' + gqCid).find('.actual_question_holder').find('form').find('#multipleChoiceQues').prop('disabled', true);
                $('#' + gqCid).find('.actual_question_holder').find('form').find('#choices_holder').find(':input').each(function(index, element) {
                    $(element).prop('disabled', true);
                });

                $('#add_question_button').show();
            });
        });
    };
});
