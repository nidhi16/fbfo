/**
 * Created by nidhi on 29/1/17.
 */

$(document).ready(function(){
   //  Ajax call to save response to response
    $('#thank-container').hide();
    $('#responseBtn').on("click", function(event){
        event.preventDefault();
        var shortResponses = [];
        $('#shortResponses').find('form').each(function (index, form_obj) {
            shortResponses.unshift({
                'questionId': $(this).find('input[type=text]').attr('question-id'),
                'textResponse': $(this).find('input[type=text]').val()
            });
        });

        var multipleChoiceResponses = [];
        $('#multipleChoiceResponses').find('form').each(function(index, element) {
            console.log($(element).attr('question-id'));
            var mcq_response = {
                'questionId': $(element).attr('question-id')
            };
            var choices = [];
            $(element).find('input[type="checkbox"]').each(function(index, input_element) {
                console.log($(input_element).val());
                console.log($(input_element).prop('checked'));
                choices.unshift({
                    'choiceId': $(input_element).val(),
                    'checked': $(input_element).prop('checked')
                });
            });
            mcq_response['choices'] = choices;
            multipleChoiceResponses.unshift(mcq_response);
        });

        var formData = {
            'form': $('#mainForm').attr('form-id'),
            'shortResponses': JSON.stringify(shortResponses),
            'multipleChoiceResponses': JSON.stringify(multipleChoiceResponses),
            'csrfmiddlewaretoken': csrf_token
        };
        console.log(formData);
        $.ajax({
            url: '/api/response/save/',
            type: 'POST',
            data: formData,
            // contentType: 'application/json'
        }).fail(function(){
            console.log("Error");
        }).done(function(data){
            console.log("success");
            console.log(data);
            $('#response-container').hide();
            $('#thank-container').show();
        });
    });

});