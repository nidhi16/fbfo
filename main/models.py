from __future__ import unicode_literals
from django.db import models


class Form(models.Model):
    title = models.CharField(max_length=80)
    description = models.CharField(max_length=128)

    def __str__(self):
        return self.title


class ShortQuestion(models.Model):
    short_question_text = models.CharField(max_length=80)
    form = models.ForeignKey(Form, on_delete=models.CASCADE)

    def __str__(self):
        return self.short_question_text


class MultipleChoiceQuestion(models.Model):
    mc_question_text = models.CharField(max_length=80)
    form = models.ForeignKey(Form, on_delete=models.CASCADE)
    other = models.BooleanField(default=True)

    def __str__(self):
        return self.mc_question_text


class MultipleChoices(models.Model):
    choice_text = models.CharField(max_length=30)
    mc_question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE)

    def __str__(self):
        return self.choice_text


class Response(models.Model):
    form = models.ForeignKey(Form, on_delete=models.CASCADE)

    def __str__(self):
        return 'Resp: {}'.format(self.form.title)


class ShortResponse(models.Model):
    short_response_text = models.CharField(max_length=128)
    response = models.ForeignKey(Response, on_delete=models.CASCADE)
    short_question = models.ForeignKey(ShortQuestion, on_delete=models.CASCADE)

    def __str__(self):
        return self.short_response_text


class MultipleChoiceResponse(models.Model):
    response = models.ForeignKey(Response, on_delete=models.CASCADE)
    mc_question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE)
    choices = models.ForeignKey(MultipleChoices, on_delete=models.CASCADE)
    accepted = models.BooleanField(default=False)

    def __str__(self):
        return '{}: {}'.format(self.accepted, self.choices.choice_text)


class MultipleChoiceOtherResponse(models.Model):
    other_response = models.CharField(max_length=30)
    response = models.ForeignKey(Response, on_delete=models.CASCADE)
    mc_question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE)

    def __str__(self):
        return self.other_response
