from django import forms
from .models import *
import json


class AppForm(forms.ModelForm):
    class Meta:
        model = Form
        fields = ['title', 'description']


class ShortQuestionForm(forms.ModelForm):
    form = forms.ModelChoiceField(queryset=Form.objects.all())

    class Meta:
        model = ShortQuestion
        fields = ['short_question_text', 'form']


class MultipleQuestionForm(forms.Form):
    form = forms.ModelChoiceField(queryset=Form.objects.all())
    multiple_choice_question_text = forms.CharField(max_length=1024)
    options = forms.CharField(max_length=4096)

    def clean_options(self):
        options_json = self.cleaned_data['options']
        try:
            json_data = json.loads(options_json)  # loads string as json
        except:
            raise forms.ValidationError("Invalid data in options")
            # if json data not valid:
            # raise forms.ValidationError("Invalid data in jsonfield")
        return json_data
