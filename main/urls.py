from django.conf.urls import url
from . import views


app_name = 'main'

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^home/$', views.home, name="home"),
    url(r'^api/form/edit/$', views.edit, name="edit"),
    url(r'^api/form/save/$', views.form_save, name="form_save"),
    url(r'^api/shortQues/save/$', views.short_ques_save, name="shortQuesSave"),
    url(r'^api/multipleQues/save/$', views.multiple__ques_save, name="multipleQuesSave"),
    url(r'^api/response/(?P<form_id>[0-9]+)/$', views.response, name="response"),
    url(r'^api/response/save/$', views.response_save, name="saveResponses"),
    url(r'^api/result/(?P<form_id>[0-9]+)/$', views.result, name="result"),
    url(r'^api/response/detail/(?P<response_id>[0-9]+)/$', views.response_detail,
        name="responseDetail"),
]
